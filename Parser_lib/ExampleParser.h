/*
 * ExampleParser.h
 *
 *  Created on: Dec 19, 2022
 *  @author Jan Łukaszewicz
 */

#ifndef EXAMPLEPARSER_H_
#define EXAMPLEPARSER_H_

#include "CXParser.h"


void UartTransmit(char *Message);

void Parser_parseLED(void);
void Parser_parseENV(void);
void Parser_parseNAME(void);

#endif /* EXAMPLEPARSER_H_ */
