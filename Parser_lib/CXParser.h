/*
 * Parser.h
 *
 *  Created on: Dec 8, 2022
 *  @author Jan Łukaszewicz
 */

#ifndef CXPARSER_H_
#define CXPARSER_H_

#include "RingBuffer.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"


#define ENDLINE  '\n'
#define SEPARATE_STRTOK "="
#define SEPARATE_CMD '='

typedef void (*CallBackCmdFun_t)(void);
typedef void (*CallBackUartTransmit_t)(char *Message);

typedef enum
{
	CX_NO_ERROR = 0,
	CX_ERROR,
	RING_BUFFER_ERROR
}CXError_t;

typedef struct
{
	char *Cmd;
	CallBackCmdFun_t Fun;

}CXParserCmd_t;

typedef struct
{
	RingBuffer_t ReceiveRingBuffor;
	uint8_t ReceiveParsedData[RING_BUFFER_SIZE];
	uint8_t ReceiveDMAData[RING_BUFFER_SIZE];

	uint8_t ReceiveOneByte;
	uint8_t  ReceiveLines;

	CXParserCmd_t *Cmd;
	size_t CmdSize;

	uint8_t DetectOfSeparateCmd;

	CallBackUartTransmit_t UartTransmit;
}CXParser_t;

uint8_t * CXP_ReceiveDMAData(CXParser_t *Parser);
uint8_t * CXP_ReceiveOneByte(CXParser_t *Parser);

CXError_t CXP_WriteByteToBuff(CXParser_t *Parser);
CXError_t CXP_WriteDMAToRingBuff(CXParser_t *Parser, uint16_t SizeFromDMA);

CXError_t CXP_Task(CXParser_t *Parser);

CXError_t CXP_Init(CXParser_t *Parser, CXParserCmd_t *Cmd, size_t CmdSize, CallBackUartTransmit_t UartTransmit);

#endif /* PARSER_H_ */
