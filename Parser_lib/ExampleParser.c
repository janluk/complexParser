/*
 * ExampleParser.c
 *
 *  Created on: Dec 19, 2022
 *  @author Jan Łukaszewicz
 */
#include "ExampleParser.h"
#include "usart.h"

static char MyName[64] = "Default";

void UartTransmit(char *Message)
{
//	HAL_UART_Transmit(&huart2, (uint8_t *)Message, strlen(Message), 1000);
	HAL_UART_Transmit_DMA(&huart2, (uint8_t *)Message,strlen(Message));
}

void Parser_parseLED(void)
{
	char *ParsePtr = strtok(NULL, ",");

	if (strlen(ParsePtr) > 0)
	{
		if (ParsePtr[0] < '0' || ParsePtr[0] > '1') // checking if incoming char is a number
		{
			UartTransmit("Wrong command. Please type 0 or 1 \r\n");
			return;
		}

		if ('1' == ParsePtr[0])
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
			UartTransmit("LED ON\n\r");
		}
		else if ('0' == ParsePtr[0])
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
			UartTransmit("LED OFF\n\r");
		}

	}
}

// ENV=
//			X,Y,Z
void Parser_parseENV(void)
{
	uint8_t i, j;
	double EnvParameters[3];
	char Message[256];

	for (i = 0; i < 3; i++)
	{
		char *ParsePtr = strtok(NULL, ",");

		if (strlen(ParsePtr) > 0)
		{
			// x.xx 	i = 0
			// y.yy 	i = 1
			// z.zz 	i = 2

			for (j = 0; ParsePtr[j] != 0; j++)
			{
				if ((ParsePtr[j] < '0' || ParsePtr[j] > '9') && ParsePtr[j] != '.')
				{
					UartTransmit("ENV wrong value, Don't use letters. \r\n");
					return;
				}
			}

			EnvParameters[i] = atof(ParsePtr);
		}
		else
		{
			UartTransmit("ENV error. ENV=X,Y,Z\\n\r\n");
			return;
		}
	}

	sprintf(Message, " Temperature %.2f\r\n Humidity %.2f\r\n Pressure %.2f\r\n", EnvParameters[0],EnvParameters[1],EnvParameters[2]);
	UartTransmit(Message);
}

// NAME=
//			STRING
//			?
void Parser_parseNAME(void)
{
	char Message[128];
	char *ParsePtr = strtok(NULL, ",");

	if(strlen(ParsePtr) >0)
	{
		if(0 == strcmp("?", ParsePtr))
		{
			sprintf(Message, "My name is %s\r\n", MyName);
			UartTransmit(Message);
		}
		else
		{
			if(strlen(ParsePtr) > 32)
			{
				UartTransmit("The Name is too long. Should by less then 32 bits.");
				return;
			}

			strcpy(MyName, ParsePtr);

			sprintf(Message, "My new name is %s\r\n", MyName);
			UartTransmit(Message);
		}
	}
	else
	{
		UartTransmit("Name can not by empty.");
	}
}
