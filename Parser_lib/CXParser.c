/*
 * Parser.c
 *
 *  Created on: Dec 8, 2022
 *  @author Jan Łukaszewicz
 */

#include "CXParser.h"


/*
 *  Check, if the data to receive parsing exists.
 */
static void CXP_ParseData(CXParser_t *Parser )
{
	uint8_t i;

	char *ParsePtr = strtok((char*) Parser->ReceiveParsedData, SEPARATE_STRTOK);

	for (i = 0; i < Parser->CmdSize; i++)
	{
		if (0 == strcmp(Parser->Cmd[i].Cmd, ParsePtr))
		{
			Parser->Cmd[i].Fun();
			return;
		}
	}

	Parser->UartTransmit("Wrong command\n\r");
}

/*
 * 	Analyzing  incoming data.
 * 	Coping data from RingBuffer to ReceiveParsedData buffer until ENDLINE was detected.
 * 	if ENDLINE was detected than it is changing to zero.
 */
static void CXP_TakeLIne(CXParser_t *Parser)
{
	uint8_t i = 0;
	uint8_t Tmp;

	do
	{
		RB_Read(&Parser->ReceiveRingBuffor, &Tmp);

		if (Tmp == ENDLINE)
		{
			Parser->ReceiveParsedData[i] = 0;
		}
		else
		{
			Parser->ReceiveParsedData[i] = Tmp;
		}
		i++;
	}
	while (Tmp != ENDLINE);
}

/*
 *  The main task that parses data
 */
CXError_t CXP_Task(CXParser_t *Parser)
{
	if (NULL != Parser)
	{
		if (Parser->ReceiveLines > 0)
		{
			CXP_TakeLIne(Parser);

			Parser->ReceiveLines--;

			CXP_ParseData(Parser);
		}
		return CX_NO_ERROR;
	}
	return CX_ERROR;
}

/*
 * 	Check ASCII Letter. If it lower case , change it into big.
 */
static void CXP_CharSmallToBig(CXParser_t *Parser, uint8_t *ByteOfChar)
{
	if (SEPARATE_CMD == (*ByteOfChar) )
	{
		Parser->DetectOfSeparateCmd = 1;
		return;
	}
	else if(ENDLINE == (*ByteOfChar) )
	{
		Parser->DetectOfSeparateCmd = 0;
		return;
	}

	if ( (0 == Parser->DetectOfSeparateCmd) && (((*ByteOfChar) >='a') && ((*ByteOfChar) <= 'z')) )
	{
			(*ByteOfChar) = (*ByteOfChar) - ('a' - 'A');
	}
}

/*
 *  Checking lower case in incoming data.
 *  Writes one incoming bytes to RingBuffer.
 *  If detects ENDLINE than increase in number of ReceiveLines.
 */
CXError_t CXP_WriteByteToRingBuff(CXParser_t *Parser)
{
	if (NULL != Parser)
	{
		CXP_CharSmallToBig(Parser, &Parser->ReceiveOneByte);

		if (RB_OK == RB_Write(&Parser->ReceiveRingBuffor, Parser->ReceiveOneByte))
		{
			if (Parser->ReceiveOneByte == ENDLINE)
			{
				Parser->ReceiveLines++;
			}
			return CX_NO_ERROR;
		}
		else
		{
			RB_Flush(&Parser->ReceiveRingBuffor);
			Parser->ReceiveLines = 0;

			return RING_BUFFER_ERROR;
		}
	}
	return CX_ERROR;
}

/*
 *  Checking lower case in incoming data from DMA buffer.
 *  Writes one incoming bytes to RingBuffer.
 *  If detects ENDLINE than increase in number of ReceiveLines.
 */
CXError_t CXP_WriteDMAToRingBuff(CXParser_t *Parser, uint16_t SizeFromDMA)
{
	if (NULL != Parser)
	{
		uint8_t i;

		for(i = 0; i < SizeFromDMA; i++ )
		{
			CXP_CharSmallToBig(Parser, &Parser->ReceiveDMAData[i]);

			if (RB_OK == RB_Write(&Parser->ReceiveRingBuffor,	Parser->ReceiveDMAData[i]))
			{
				if (Parser->ReceiveDMAData[i] == ENDLINE)
				{
					Parser->ReceiveLines++;
				}
			}
			else
			{
				RB_Flush(&Parser->ReceiveRingBuffor);
				Parser->ReceiveLines = 0;

				return RING_BUFFER_ERROR;
			}
		}
		return CX_NO_ERROR;
	}
	return CX_ERROR;
}

/*
 *  Return address to variable, where write incoming byte in structure.
 */
uint8_t * CXP_ReceiveOneByte(CXParser_t *Parser)
{
	if (NULL != Parser)
	{
		return &Parser->ReceiveOneByte;
	}
	return NULL;
}

/*
 *  Return address to Buffer, where write incoming bytes from DMA.
 */
uint8_t * CXP_ReceiveDMAData(CXParser_t *Parser)
{
	if (NULL != Parser)
	{
		return Parser->ReceiveDMAData;
	}
	return NULL;
}

/*
 *  Initialize CXParser_t structure, CXParserCmd_t structure, CmdSize structure, CallBackUartTransmit Callback.
 */
CXError_t CXP_Init(CXParser_t *Parser, CXParserCmd_t *Cmd, size_t CmdSize, CallBackUartTransmit_t UartTx)
{

	if ((NULL != Parser) && (NULL != Cmd) && (NULL != UartTx))
	{
		Parser->Cmd = Cmd;
		Parser->CmdSize = CmdSize;
		Parser->DetectOfSeparateCmd = 0;
		Parser->UartTransmit = UartTx;

		return CX_NO_ERROR;
	}
	else
	{
		  while (1); // check register callback
	}
	return CX_ERROR;

}




