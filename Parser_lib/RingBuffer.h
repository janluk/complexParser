/*
 * RingBuffer.h
 *
 *  Created on: Nov 27, 2022
 *  @author Jan Łukaszewicz
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

#include "stdint.h"

#define RING_BUFFER_SIZE 64

// success status
typedef enum
{
	RB_OK = 0,
	RB_ERROR = 1

}RB_Status;

// Object Ring Buffer
typedef struct
{
	uint16_t Head;
	uint16_t Tail;
	uint8_t Buffer[RING_BUFFER_SIZE];
}RingBuffer_t;


// functions

//write
RB_Status RB_Write(RingBuffer_t *Buf, uint8_t Value);

//read
RB_Status RB_Read(RingBuffer_t *Buf, uint8_t *Value);

//flush
void RB_Flush(RingBuffer_t *Buf);



#endif /* RINGBUFFER_H_ */
