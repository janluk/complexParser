# Parser library

* [General info](#general-info)
* [Setup](#setup)
* [Examples functions](#example-functions)
* [Author](#author)



# **General info** 
Complex parser with a size recognition of characters ASCII. </br>
Tested on STM32 NUCLEO-F411RE

# **Setup** 


## [1] Include files
```C
		#include "CXParser.h"
		#include "ExampleParser.h"
```

## [2] Initialize structures

 Main structures:

```C
		CXParser_t Parser;
```

 Command structures:
```C
		CXParserCmd_t ParserCmd[]={
			{"LED", Parser_parseLED },		// LED=1 or 0
			{"ENV", Parser_parseENV },		// ENV=1.34,2.33,4.44
			{"NAME", Parser_parseNAME },	// NAME=? NAME=ANOTHER NAME
		};

		// size of structure
		#define PARSER_CMD_SIZE ( sizeof(ParserCmd) / sizeof(ParserCmd[0]) )
```

## [3] Initialized UART
</br>

<details>
<summary>Click here to see <b>UART Interrupt</b></summary>


### Initialized
```C
  		HAL_UART_Receive_IT(&huart2, CXP_ReceiveOneByte(&Parser),1);
```  

### CallBack
```C
		void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
		{
			if(huart->Instance == USART2)
			{
				if(CX_NO_ERROR == CXP_WriteByteToRingBuff(&Parser))
				{
					HAL_UART_Receive_IT(&huart2, CXP_ReceiveOneByte(&Parser),1);
				}
			}
		}

```

</details>

<details>
<summary>Click here to see <b>DMA UART Interrupt</b></summary>


### Initialized
```C
		HAL_UARTEx_ReceiveToIdle_DMA(&huart2, CXP_ReceiveDMAData(&Parser), RING_BUFFER_SIZE);
```

### DMA UART Callback
```C
		void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
		{
			if(huart->Instance == USART2)
			{
				if(CX_NO_ERROR == CXP_WriteDMAToRingBuff(&Parser, Size))
				{

					HAL_UARTEx_ReceiveToIdle_DMA(&huart2, CXP_ReceiveDMAData(&Parser), RING_BUFFER_SIZE);
				}
				else
				{
					char uartMessage[128];

					sprintf(uartMessage, "ERROR: File %s, FN: %s, line %d\n", __FILE__, __func__, __LINE__);
					UartTransmit(uartMessage);
				}
			}
		}
```


</details>
</br>

## [4] Initialized library

```C
CXP_Init(&Parser, ParserCmd, PARSER_CMD_SIZE, &UartTransmit);
```

## [5] Main loop:
```C
	  CXP_Task(&Parser);
```

# **Example functions**

<details>
<summary>Click here to see <b>ExampleParser.h</b></summary>

```C
#ifndef EXAMPLEPARSER_H_
#define EXAMPLEPARSER_H_

#include "CXParser.h"


void UartTransmit(char *Message);

void Parser_parseLED(void);
void Parser_parseENV(void);
void Parser_parseNAME(void);

#endif /* EXAMPLEPARSER_H_ */
```

</details>


<details>
<summary>Click here to see <b>ExampleParser.c</b></summary>

```C++
#include "ExampleParser.h"
#include "usart.h"

static char MyName[64] = "Default";

void UartTransmit(char *Message)
{
//	HAL_UART_Transmit(&huart2, (uint8_t *)Message, strlen(Message), 1000);
	HAL_UART_Transmit_DMA(&huart2, (uint8_t *)Message,strlen(Message));
}

void Parser_parseLED(void)
{
	char *ParsePtr = strtok(NULL, ",");

	if (strlen(ParsePtr) > 0)
	{
		if (ParsePtr[0] < '0' || ParsePtr[0] > '1') // checking if incoming char is a number
		{
			UartTransmit("Wrong command. Please type 0 or 1 \r\n");
			return;
		}

		if ('1' == ParsePtr[0])
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
			UartTransmit("LED ON\n\r");
		}
		else if ('0' == ParsePtr[0])
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
			UartTransmit("LED OFF\n\r");
		}

	}
}

// ENV=
//			X,Y,Z
void Parser_parseENV(void)
{
	uint8_t i, j;
	double EnvParameters[3];
	char Message[256];

	for (i = 0; i < 3; i++)
	{
		char *ParsePtr = strtok(NULL, ",");

		if (strlen(ParsePtr) > 0)
		{
			// x.xx 	i = 0
			// y.yy 	i = 1
			// z.zz 	i = 2

			for (j = 0; ParsePtr[j] != 0; j++)
			{
				if ((ParsePtr[j] < '0' || ParsePtr[j] > '9') && ParsePtr[j] != '.')
				{
					UartTransmit("ENV wrong value, Don't use letters. \r\n");
					return;
				}
			}

			EnvParameters[i] = atof(ParsePtr);
		}
		else
		{
			UartTransmit("ENV error. ENV=X,Y,Z\\n\r\n");
			return;
		}
	}

	sprintf(Message, " Temperature %.2f\r\n Humidity %.2f\r\n Pressure %.2f\r\n", EnvParameters[0],EnvParameters[1],EnvParameters[2]);
	UartTransmit(Message);
}

// NAME=
//			STRING
//			?
void Parser_parseNAME(void)
{
	char Message[128];
	char *ParsePtr = strtok(NULL, ",");

	if(strlen(ParsePtr) >0)
	{
		if(0 == strcmp("?", ParsePtr))
		{
			sprintf(Message, "My name is %s\r\n", MyName);
			UartTransmit(Message);
		}
		else
		{
			if(strlen(ParsePtr) > 32)
			{
				UartTransmit("The Name is too long. Should by less then 32 bits.");
				return;
			}

			strcpy(MyName, ParsePtr);

			sprintf(Message, "My new name is %s\r\n", MyName);
			UartTransmit(Message);
		}
	}
	else
	{
		UartTransmit("Name can not by empty.");
	}
}
```
</details>
</br>

# **Author**

* Jan Łukaszewicz pldevluk@gmail.com